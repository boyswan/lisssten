

$(function() {

  $(window).on('resize', function(){

    $('.handle').css({'height': $(this).height() });
    $('.container').css({'width': $(this).width() });

  }).trigger('resize');

//=======================================================//
 
  var masterVolume = [];
  var volumeControl = function(i, val, y){
      $('#jquery_jplayer_' + i).jPlayer("volume", parseFloat(y.toFixed(1)));
      masterVolume[i] = y.toFixed(1);
  };  

//=======================================================//

  var volumeCallback = function(event){

    var volume = event.jPlayer.options.volume;
    var playing = !event.jPlayer.status.paused;
    var paused = event.jPlayer.status.paused;

    if ((volume > 0 && playing) || (volume == 0 && paused)){
      return false;
    }

    var masterVolumeStatus = false;
    
    for (i=1; i<=masterVolume.length; i++){
      if(masterVolume[i] > 0){
        masterVolumeStatus = true;
      }
    }

    if (masterVolumeStatus){
      $('.info-animation').addClass('on');
      console.log('tree');
    } else {
      $('.info-animation').removeClass('on');
      $('.info-animation').addClass('off');
      setTimeout(function() {
        $('.info-animation').removeClass('off');
      }, 100);
    } 

    var currentTime = event.jPlayer.status.currentTime;
    var opt = (volume == 0) ? "pause" : "play";
    $(this).jPlayer(opt, currentTime);

  }


//========================================================//


  $('.block').each(function(index, val){

    new Dragdealer($(val).attr('id'), {
      vertical: true,
      horizontal: false,

      animationCallback: function(x, y) {
        var i = $(val).index('.block') + 1;
        volumeControl(i, x, y);
      } 

    });

  }); 


//========================================================//


  $('.jp-jplayer').each(function(index, val){

    $(this).jPlayer({

      ready: function() {

        $(this).jPlayer("setMedia", {
          mp3: 'audio/file'+ (index + 1) +'.mp3',
        })
        .bind($.jPlayer.event.volumechange, volumeCallback);

      },    
      solution: "html, flash",
      supplied: "mp3",
      preload: "auto",
      swfPath: 'js',
      loop: true,
    });

  });


//=========================================================//

  $( "#info" ).click(function() {
    $(this).toggleClass('toggle')
    $('.info-detail').toggleClass('toggle');
    $(".info-fill").toggleClass('toggle')
    $(".info-text").toggleClass('toggle')
    $(".header").toggleClass('toggle')
    $(".fb-iframe").toggleClass('toggle')
    $('.info-animation span').toggleClass('toggle');
  });

  $('.block').hover(function(){
    function hideArrows(){
        $('.first--info__wrap').addClass('toggle');
        $('.small-arrow--up').addClass('toggle');
    }
    setTimeout(hideArrows, 400);
  });

  $(document).bind(
    'touchmove',
    function(e) {
      e.preventDefault();
    }
  );


});


